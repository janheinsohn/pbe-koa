import * as Koa from 'koa'
import * as Router from 'koa-router';
import logger = require('koa-logger');
import bodyParser = require('koa-bodyparser');
import aufgabenbezeichnungenRouter from './routes/aufgabenbezeichnungen';

const app = new Koa();
const router = new Router();

//Middleware
app.use(logger());
app.use(bodyParser());

//Routing
router.get('/', async (ctx) => {
    ctx.body = 'PBE Running';
});

//Routes mit der App-Verknüpfen
app.use(router.routes());
app.use(aufgabenbezeichnungenRouter.routes());

//Start
app.listen(3000);

console.log('PBE Server running... Port: 3000');