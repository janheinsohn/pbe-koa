import { IAufgabenbezeichnungRepository } from "./aufgabenbezeichnungRepository";
import { Aufgabenbezeichnung } from "../models/aufgabenbezeichnung";

export class InMemoryAufgabenbezeichnungRepository implements IAufgabenbezeichnungRepository {

    private _aufgabenbezeichnungen: Aufgabenbezeichnung[] = [];
    private _nextId = 1;

    private getNextId() {
        return this._nextId++;
    }

    async getAll(): Promise<Aufgabenbezeichnung[]> {
        return this._aufgabenbezeichnungen;
    }   
    
    async get(id: number): Promise<Aufgabenbezeichnung> {
        const aufgabenbezeichnung = this._aufgabenbezeichnungen.find(x => x.id === id);
        return aufgabenbezeichnung;
    }

    async create(obj: Aufgabenbezeichnung): Promise<Aufgabenbezeichnung> {
        obj.id = this.getNextId();
        this._aufgabenbezeichnungen.push(obj);
        return obj;
    }

    async update(id: number, obj: Aufgabenbezeichnung): Promise<Aufgabenbezeichnung> {
        let aufgabenbezeichnung = this._aufgabenbezeichnungen.filter(x => x.id === id)[0];
        aufgabenbezeichnung.bezeichnung = obj.bezeichnung;
        return aufgabenbezeichnung;
    }

    async delete(id: number): Promise<Aufgabenbezeichnung> {
        const aufgabenbezeichnungIndex = this._aufgabenbezeichnungen.findIndex(x => x.id === id);
        const aufgabenbezeichnung = this._aufgabenbezeichnungen[aufgabenbezeichnungIndex];
        this._aufgabenbezeichnungen.splice(aufgabenbezeichnungIndex, 1);
        return aufgabenbezeichnung;
    }


}