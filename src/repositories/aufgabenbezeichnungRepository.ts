import { Aufgabenbezeichnung } from "../models/aufgabenbezeichnung";
import { IRepository } from "./repository";

export interface IAufgabenbezeichnungRepository extends IRepository<Aufgabenbezeichnung> {

}