export interface IRepository<T> {
    getAll(): Promise<T[]>;
    get(id: number): Promise<T>;
    create(obj: T): Promise<T>;
    update(id: number, obj: T): Promise<T>;
    delete(id: number): Promise<T>;
}