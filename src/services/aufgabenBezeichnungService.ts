import { IAufgabenbezeichnungRepository } from "../repositories/aufgabenbezeichnungRepository";
import { Aufgabenbezeichnung } from "../models/aufgabenbezeichnung";

export class AufgabenbezeichnungService {
    private _repo: IAufgabenbezeichnungRepository;

    constructor(repo: IAufgabenbezeichnungRepository) {
        this._repo = repo;
    }

    async getAllAufgabenbezeichnung(): Promise<Aufgabenbezeichnung[]> {
        console.log("Lade alle Aufgabenbezeichnungen");
        return await this._repo.getAll();
    }

    async getAufgabenbezeichnung(id: number): Promise<Aufgabenbezeichnung> {
        return await this._repo.get(id);
    }

    async createAufgabenbezeichnung(aufgabenbezeichnung: Aufgabenbezeichnung): Promise<Aufgabenbezeichnung> {
        return await this._repo.create(aufgabenbezeichnung);
    }

    async updateAufgabenbezeichnung(id: number, aufgabenbezeichnung: Aufgabenbezeichnung): Promise<Aufgabenbezeichnung> {
        return await this._repo.update(id, aufgabenbezeichnung);
    }

    async deleteAufgabenbezeichnung(id: number): Promise<Aufgabenbezeichnung> {
        return await this._repo.delete(id);
    }
}