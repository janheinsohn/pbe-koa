import * as Router from 'koa-router';
import { AufgabenbezeichnungService } from '../services/aufgabenBezeichnungService';
import { InMemoryAufgabenbezeichnungRepository } from '../repositories/inMemoryAufgabenbezeichnungRepository';
import { Aufgabenbezeichnung } from '../models/aufgabenbezeichnung';

const router = new Router();
const service = new AufgabenbezeichnungService(new InMemoryAufgabenbezeichnungRepository());

router.get('/aufgabenbezeichnungen', async (ctx) => {
    const aufgabenbezeichnungen = await service.getAllAufgabenbezeichnung();
    ctx.body = aufgabenbezeichnungen;
});

router.get('/aufgabenbezeichnungen/:id', async (ctx) => {
    const aufgabenbezeichnung = await service.getAufgabenbezeichnung(Number(ctx.params.id));
    if(aufgabenbezeichnung)
        ctx.body = aufgabenbezeichnung;
    else 
        ctx.throw(404);
});

router.post('/aufgabenbezeichnungen', async (ctx) => {
    const aufgabenbezeichnung = <Aufgabenbezeichnung> ctx.request.body;
    const newAufgabenbezeichnung = await service.createAufgabenbezeichnung(aufgabenbezeichnung);
    ctx.body = newAufgabenbezeichnung;
    ctx.status = 201;

});

router.put('/aufgabenbezeichnungen/:id', async (ctx) => {
    const aufgabenbezeichnung = <Aufgabenbezeichnung> ctx.request.body;
    const newAufgabenbezeichnung = await service.updateAufgabenbezeichnung(Number(ctx.params.id), aufgabenbezeichnung);
});

router.delete('/aufgabenbezeichnungen/:id', async (ctx) => {
    const aufgabenbezeichnung = await service.deleteAufgabenbezeichnung(Number(ctx.params.id));
    ctx.body = aufgabenbezeichnung;
});

export default router;